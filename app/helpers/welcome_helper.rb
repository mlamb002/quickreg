module WelcomeHelper

  #takes in a term_course object and outputs days of the week for that course in a string
  #assumes
  def day_time_of_week(term_course)
    temp_days = ""
    temp_time = ""
    got_time = false

    #sunday
    if(!term_course.sunday_start.nil?)
      temp_days += "Su"
      if !got_time
        temp_time += term_course.sunday_start.to_s
      end
    end
    if(!term_course.sunday_end.nil?)
      temp_time += "-#{term_course.sunday_end}"
      got_time = true
    end

    #monday
    if(!term_course.monday_start.nil? and !got_time)
      temp_days += "Mo"
      if !got_time
        temp_time += term_course.monday_start.to_s
      end
    end
    if(!term_course.monday_end.nil? and !got_time)
      temp_time += "-#{term_course.monday_end}"
      got_time = true
    end

    #tuesday
    if(!term_course.tuesday_start.nil?)
      temp_days += "Tu"
      if !got_time
        temp_time += term_course.tuesday_start.to_s
      end
    end
    if(!term_course.tuesday_end.nil? and !got_time)
      temp_time += "-#{term_course.tuesday_end}"
      got_time = true
    end

    #wednesday
    if(!term_course.wednesday_start.nil?)
      temp_days += "We"
      if !got_time
        temp_time += term_course.wednesday_start.to_s
      end
    end
    if(!term_course.wednesday_end.nil? and !got_time)
      temp_time += "-#{term_course.wednesday_end}"
      got_time = true
    end

    #thursday
    if(!term_course.thursday_start.nil?)
      temp_days += "Th"
      if !got_time
        temp_time += term_course.thursday_start.to_s
      end
    end
    if(!term_course.thursday_end.nil? and !got_time)
      temp_time += "-#{term_course.thursday_end}"
      got_time = true
    end

    #friday
    if(!term_course.friday_start.nil?)
      temp_days += "Fr"
      if !got_time
        temp_time += term_course.friday_start.to_s
      end
    end
    if(!term_course.friday_end.nil? and !got_time)
      temp_time += "-#{term_course.friday_end}"
      got_time = true
    end

    #saturday
    if(!term_course.saturday_start.nil?)
      temp_days += "Fr"
      if !got_time
        temp_time += term_course.saturday_start.to_s
      end
    end
    if(!term_course.saturday_end.nil? and !got_time)
      temp_time += "-#{term_course.saturday_end}"
      got_time = true
    end

    temp_days += " #{temp_time}"
  end



end



