class User < ApplicationRecord
  validates :schoolId, presence: true
  validates :firstName, presence: true
  validates :lastName, presence: true
  validates :userType, presence: true
  validates :email, presence: true, uniqueness: true, format: { with: /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i, message: "invalid e-mail address"}
  validates :lastAccessed, presence: true
  has_many :user_majors, :dependent => :destroy
  has_many :user_courses, :dependent => :destroy
end
