class Term < ApplicationRecord
  has_many :term_courses, :dependent => :destroy
end
