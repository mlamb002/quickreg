class RequirementCategory < ApplicationRecord
  has_many :requirement_category_majors, :dependent => :destroy
  has_many :requirement_category_courses, :dependent => :destroy
end
