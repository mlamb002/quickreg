class Major < ApplicationRecord
  has_many :user_majors, :dependent => :destroy
  has_many :requirement_category_majors, :dependent => :destroy
end
