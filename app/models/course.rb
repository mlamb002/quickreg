class Course < ApplicationRecord
  has_many :term_courses, :dependent => :destroy
  has_many :requirement_category_courses, :dependent => :destroy
  has_many :user_courses, :dependent => :destroy
end
