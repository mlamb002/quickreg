class RequirementCategoryMajor < ApplicationRecord
  #note needs to be plural because app was looking for requirement_categories but belongs_to :requirement_category
  #was generating requirement_categorys
  belongs_to :requirement_category
  belongs_to :major
end
