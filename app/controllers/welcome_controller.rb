class WelcomeController < ApplicationController
  before_action :authenticate_session, only: [:db_info, :profile, :course_overview, :major_requirement]

  def login
    if !params['sid'].nil?

      temp_user = User.find_by(schoolId: params['sid'])

      if !temp_user.nil?
        if params['pass'] == temp_user.password_digest
          session[:current_user_id] = temp_user.id
          session[:current_user_name] = temp_user.firstName
          flash[:success] = "Login success. Welcome " + temp_user.firstName
          redirect_to(course_overview_path) and return
        else
          flash.now[:alert] = "Incorrect username or password combination."
        end
      else
        flash.now[:alert] = "Incorrect username or password combination."
      end

    end

  end

  def db_info

  end

  def sub_landing

  end

  def course_overview
    @info = Hash.new()

    @info['terms'] = get_info_coqf

    #goes in here if query was submitted
    if !params['term'].nil?
      @info['cur_user'] = User.find_by(id: session[:current_user_id])

      #get major
      @info['major'] = Major.find_by(name: params['major'], catalogYear: params['catalog_year'])

      #get user course history
      user_course_ids = @info['cur_user'].user_courses.distinct.pluck(:course_id)
      @temp = Course.find(user_course_ids)

      #get current term information
      term_queried = params['term'].split(" ")
      cur_term = Term.find_by(name: term_queried[0], year: term_queried[1])

      #get requirement categories
      req_ids = @info['major'].requirement_category_majors.distinct.pluck(:requirement_category_id)
      @info['req_cat'] = RequirementCategory.order(:category,:name).find(req_ids)

      @info['co'] = Hash.new

      #roll through categories
      @info['req_cat'].each do | category |
        units_left = category.unitFulfillmentRequirement

        #get courses for this category
        temp_courses_id = category.requirement_category_courses.distinct.pluck(:course_id)
        temp_courses = Course.find(temp_courses_id)

        have_courses = Array.new()
        have_not_courses = Array.new()
        have_not_term_courses = Array.new()
        req_fin = false

        #roll through courses in a category
        temp_courses.each do | course |

          if user_course_ids.include? course.id
            #user has course

            units_left -= course.units
            user_course_ids.delete(course.id)
            have_courses.push(course)

          else
            #user doesn't have course
            have_not_courses.push(course)

          end

          if units_left <= 0
            #unit requirement is fulfilled no need to go through anymore courses
            req_fin = true
            break
          end

        end

        #if requirements are not fulfilled need course schedules of classes for term
        #queried
        if !req_fin
          have_not_term_courses = cur_term.term_courses.where(:course_id => have_not_courses.map{|e| e.id}).to_a
        end

        @info['co'][category.id] = {:units_left => units_left, :fulfilled => req_fin, :have_courses => have_courses, :have_not_courses => have_not_courses, :have_not_term_courses => have_not_term_courses}

      end


    end






  end

  def profile
    @profile = User.find_by(id: session[:current_user_id])
    @majors = @profile.user_majors
    @courses = @profile.user_courses
    params['unit_count'] = get_unit_count(@courses)
  end

  def major_requirement
    #goes in here if query was submitted
    if !params['major'].nil?
      @info = Hash.new()

      @info['cur_user'] = User.find_by(id: session[:current_user_id])

      #get major
      @info['major'] = Major.find_by(name: params['major'], catalogYear: params['catalog_year'])

      #get user course history
      user_course_ids = @info['cur_user'].user_courses.distinct.pluck(:course_id)
      @temp = Course.find(user_course_ids)

      #get requirement categories
      req_ids = @info['major'].requirement_category_majors.distinct.pluck(:requirement_category_id)
      @info['req_cat'] = RequirementCategory.order(:category,:name).find(req_ids)

      @info['co'] = Hash.new

      #roll through categories
      @info['req_cat'].each do | category |
        units_left = category.unitFulfillmentRequirement

        #get courses for this category
        temp_courses_id = category.requirement_category_courses.distinct.pluck(:course_id)
        temp_courses = Course.find(temp_courses_id)

        have_courses = Array.new()
        have_not_courses = Array.new()
        have_not_term_courses = Array.new()
        req_fin = false

        #roll through courses in a category
        temp_courses.each do | course |

          if user_course_ids.include? course.id and !req_fin
            #user has course

            units_left -= course.units
            user_course_ids.delete(course.id)
            have_courses.push(course)

            if units_left <= 0
              #unit requirement is fulfilled no need to go through anymore courses
              req_fin = true
            end

          else
            #user doesn't have course
            have_not_courses.push(course)

          end

        end

        @info['co'][category.id] = {:units_left => units_left, :fulfilled => req_fin, :have_courses => have_courses, :have_not_courses => have_not_courses}
      end
    end

  end

  private

  def authenticate_session
    if session[:current_user_id].nil?
      flash[:alert] = "User not authorized to access page, please log in first."
      redirect_to (login_path) and return
    end
  end

  #get course overview query form
  def get_info_coqf
    temp = Array.new()
    Term.distinct.order(:name).pluck(:name, :year).each do |name, year|
      temp.push(name + ' ' + year.to_s)
    end
    temp
  end

  #user courses is active record association of courses
  def get_unit_count(user_course)
    unit_count = 0
    Course.find( user_course.pluck(:course_id) ).each do |course|
      unit_count += course.units
    end
   unit_count
  end


end
