Rails.application.routes.draw do
  #get 'welcome/login'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get 'db_info' => 'welcome#db_info'

  get 'course_overview' => 'welcome#course_overview'

  get 'sub_landing' => 'welcome#sub_landing'

  post 'auth' => 'welcome#auth'

  get 'major_requirement' => 'welcome#major_requirement'

  post 'login' => 'welcome#login'

  get 'login' => 'welcome#login'

  get 'profile' => 'welcome#profile'

  #set the root location
  root 'welcome#login'

end
