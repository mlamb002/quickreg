class CreateDb < ActiveRecord::Migration[5.1]
  def change
    #create a table for users
    create_table :users do |t|
      t.bigint :schoolId, index: {unique: true}
      t.string :firstName
      t.string :lastName
      t.string :userType
      t.string :email, index: {unique: true}
      t.datetime :lastAccessed
      t.decimal :units
      t.integer :gradCatalogYear
      t.string :password_digest
      t.timestamps
    end

    #create a table for majors
    create_table :majors do |t|
      t.string :name
      t.integer :catalogYear
      t.timestamps
    end

    #this is to prevent the same major name in a catalog year
    add_index :majors, [:name, :catalogYear], unique: true

    #create a table for users to own majors
    create_table :user_majors do |t|
      t.belongs_to :user
      t.belongs_to :major
      t.timestamps
    end

    #this is to prevent a user from adding more than 1 of the same major
    add_index :user_majors, [:user_id, :major_id], unique: true

    #create a table for Requirement Category
    create_table :requirement_categories do |t|
      t.string :name
      t.string :category
      t.decimal :unitFulfillmentRequirement
      t.timestamps
    end

    add_index :requirement_categories, [:name, :category], unique: true

    #create a table for a major to own a requirement category
    create_table :requirement_category_majors do |t|
      t.belongs_to :major
      t.belongs_to :requirement_category
      t.timestamps
    end

    add_index :requirement_category_majors, [:major_id, :requirement_category_id], unique: true, name: 'requirement_category_majors_on_major_and_req_id'

    #create a table for courses
    create_table :courses do |t|
      t.string :name, index: {unique: true}
      t.string :descr
      t.string :genre
      t.decimal :units
      t.decimal :classStandingUnitRequirement
      t.string :divisionType
      t.timestamps
    end

    #create a table for users to own courses
    create_table :user_courses do |t|
      t.belongs_to :user
      t.belongs_to :course
      t.timestamps
    end

    add_index :user_courses, [:user_id, :course_id], unique: true

    #create a table to for requirement_categories to own courses
    create_table :requirement_category_courses do |t|
      t.belongs_to :course
      t.belongs_to :requirement_category
      t.timestamps
    end

    add_index :requirement_category_courses, [:course_id, :requirement_category_id], unique: true, name: 'requirement_category_courses_on_major_and_course_id'

    #create a table for terms
    create_table :terms do |t|
      t.string :name
      t.integer :year
      t.date :start_date
      t.date :end_date
      t.boolean :isCurrentTerm
      t.timestamps
    end

    #this is to prevent multiple terms in the same year
    add_index :terms, [:name, :year], unique: true

    #create a table for term's courses
    create_table :term_courses do |t|
     t.belongs_to :course
     t.belongs_to :term
     t.string :status
     t.string :courseType
     t.string :location
     t.string :instructor
     t.integer :section
     t.integer :classNumber
     t.integer :sunday_start
     t.integer :sunday_end
     t.integer :monday_start
     t.integer :monday_end
     t.integer :tuesday_start
     t.integer :tuesday_end
     t.integer :wednesday_start
     t.integer :wednesday_end
     t.integer :thursday_start
     t.integer :thursday_end
     t.integer :friday_start
     t.integer :friday_end
     t.integer :saturday_start
     t.integer :saturday_end
     t.timestamps
    end

    #this prevents from adding more than 1 of the same section to a course in a term
    add_index :term_courses, [:term_id, :course_id, :section], unique: true

  end
end
