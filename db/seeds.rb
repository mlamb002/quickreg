# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
#Examples of active record query

#User.create(schoolId: ,firstName: ,lastName: ,userType: ,email: , lastAccessed: ,password_digest )
#User.find_by(schoolId: 111111111).UserMajor.create(major_id: Major.find_by(name"Computer Science").id)
#User.find_by(schoolId: 333333333).user_majors.first.major.name

#delete a record and all dependencies
#temp_req_cat = RequirementCategory.find_by(name: "1. Mathematics" ,category: "B. Natural Sciences Breadth Area")
#temp_req_cat.destroy

#temp_major = Major.find_by(name: "B.S. Computer Science", catalogYear: 2017)
#temp_major.destroy

#add a major to a user
def add_major_to_user(temp_user, name, catalog_year)
  temp_user_major = temp_user.user_majors.create
  temp_major = Major.find_by(name: name, catalogYear: catalog_year)
  temp_user_major.major = temp_major
  temp_user_major.save!
end

#Add a class to a user
def add_course_to_user(temp_user, name)
  temp_user_course = temp_user.user_courses.create
  temp_course = Course.find_by(name: name)
  temp_user_course.course = temp_course
  temp_user_course.save!
end

#Add a requirement category to a major
def add_req_cat_to_major(temp_major, name, category)
  temp_req_cat_major = temp_major.requirement_category_majors.create
  temp_req_cat = RequirementCategory.find_by(name: name ,category: category)
  temp_req_cat_major.requirement_category = temp_req_cat
  temp_req_cat_major.save!
end

#Add a course to a requirement category
#temp_req_cat is requirement category object
#course is course string name
def add_course_to_req_cat(temp_req_cat, course)
  temp_course = Course.find_by(name: course)
  temp_req_cat_course = temp_req_cat.requirement_category_courses.create
  temp_req_cat_course.course = temp_course
  temp_req_cat_course.save!
end

#Add general education requirements to major
#temp_major is major object
def add_gen_ed_req_2017(temp_major)
  temp_req_cat_major = temp_major.requirement_category_majors.create
  temp_req_cat = RequirementCategory.find_by(name: "1. Written Communication" ,category: "A. Basic Skills Category")
  temp_req_cat_major.requirement_category = temp_req_cat
  temp_req_cat_major.save!

  temp_req_cat_major = temp_major.requirement_category_majors.create
  temp_req_cat = RequirementCategory.find_by(name: "2. Oral Communication" ,category: "A. Basic Skills Category")
  temp_req_cat_major.requirement_category = temp_req_cat
  temp_req_cat_major.save!

  temp_req_cat_major = temp_major.requirement_category_majors.create
  temp_req_cat = RequirementCategory.find_by(name: "3. Mathematics" ,category: "A. Basic Skills Category")
  temp_req_cat_major.requirement_category = temp_req_cat
  temp_req_cat_major.save!

  temp_req_cat_major = temp_major.requirement_category_majors.create
  temp_req_cat = RequirementCategory.find_by(name: "4. Critical Thinking" ,category: "A. Basic Skills Category")
  temp_req_cat_major.requirement_category = temp_req_cat
  temp_req_cat_major.save!

  temp_req_cat_major = temp_major.requirement_category_majors.create
  temp_req_cat = RequirementCategory.find_by(name: "1. Mathematics" ,category: "B. Natural Sciences Breadth Area")
  temp_req_cat_major.requirement_category = temp_req_cat
  temp_req_cat_major.save!

  temp_req_cat_major = temp_major.requirement_category_majors.create
  temp_req_cat = RequirementCategory.find_by(name: "2. Life Sciences" ,category: "B. Natural Sciences Breadth Area")
  temp_req_cat_major.requirement_category = temp_req_cat
  temp_req_cat_major.save!

  temp_req_cat_major = temp_major.requirement_category_majors.create
  temp_req_cat = RequirementCategory.find_by(name: "3. Physical Sciences" ,category: "B. Natural Sciences Breadth Area")
  temp_req_cat_major.requirement_category = temp_req_cat
  temp_req_cat_major.save!

  temp_req_cat_major = temp_major.requirement_category_majors.create
  temp_req_cat = RequirementCategory.find_by(name: "4. Special Topics in Science and Technology" ,category: "B. Natural Sciences Breadth Area")
  temp_req_cat_major.requirement_category = temp_req_cat
  temp_req_cat_major.save!

  temp_req_cat_major = temp_major.requirement_category_majors.create
  temp_req_cat = RequirementCategory.find_by(name: "5. Integrative Capstone in Natural Sciences" ,category: "B. Natural Sciences Breadth Area")
  temp_req_cat_major.requirement_category = temp_req_cat
  temp_req_cat_major.save!

  temp_req_cat_major = temp_major.requirement_category_majors.create
  temp_req_cat = RequirementCategory.find_by(name: "1. Arts" ,category: "C. Humanities Breadth Area")
  temp_req_cat_major.requirement_category = temp_req_cat
  temp_req_cat_major.save!

  temp_req_cat_major = temp_major.requirement_category_majors.create
  temp_req_cat = RequirementCategory.find_by(name: "2. Literature" ,category: "C. Humanities Breadth Area")
  temp_req_cat_major.requirement_category = temp_req_cat
  temp_req_cat_major.save!

  temp_req_cat_major = temp_major.requirement_category_majors.create
  temp_req_cat = RequirementCategory.find_by(name: "3. Foreign Language or Literature in Translation" ,category: "C. Humanities Breadth Area")
  temp_req_cat_major.requirement_category = temp_req_cat
  temp_req_cat_major.save!

  temp_req_cat_major = temp_major.requirement_category_majors.create
  temp_req_cat = RequirementCategory.find_by(name: "4. Philosophy" ,category: "C. Humanities Breadth Area")
  temp_req_cat_major.requirement_category = temp_req_cat
  temp_req_cat_major.save!

  temp_req_cat_major = temp_major.requirement_category_majors.create
  temp_req_cat = RequirementCategory.find_by(name: "5. Integrative Capstone in the Humanities" ,category: "C. Humanities Breadth Area")
  temp_req_cat_major.requirement_category = temp_req_cat
  temp_req_cat_major.save!

  temp_req_cat_major = temp_major.requirement_category_majors.create
  temp_req_cat = RequirementCategory.find_by(name: "1. American History and Civilization" ,category: "D. Social and Behavioral Sciences")
  temp_req_cat_major.requirement_category = temp_req_cat
  temp_req_cat_major.save!

  temp_req_cat_major = temp_major.requirement_category_majors.create
  temp_req_cat = RequirementCategory.find_by(name: "2. American Institutions" ,category: "D. Social and Behavioral Sciences")
  temp_req_cat_major.requirement_category = temp_req_cat
  temp_req_cat_major.save!

  temp_req_cat_major = temp_major.requirement_category_majors.create
  temp_req_cat = RequirementCategory.find_by(name: "3. World Cultures" ,category: "D. Social and Behavioral Sciences")
  temp_req_cat_major.requirement_category = temp_req_cat
  temp_req_cat_major.save!

  temp_req_cat_major = temp_major.requirement_category_majors.create
  temp_req_cat = RequirementCategory.find_by(name: "4. Discipline Perspectives" ,category: "D. Social and Behavioral Sciences")
  temp_req_cat_major.requirement_category = temp_req_cat
  temp_req_cat_major.save!

  temp_req_cat_major = temp_major.requirement_category_majors.create
  temp_req_cat = RequirementCategory.find_by(name: "5. Integrative Capstone in the Social and Behavioral Sciences")
  temp_req_cat_major.requirement_category = temp_req_cat
  temp_req_cat_major.save!

end

#create a User
#User.create(schoolId: ,firstName: ,lastName: ,userType: ,email: , lastAccessed: ,password_digest:, units:, gradCatalogYear:)
User.create(schoolId: 1 ,firstName: "Hyuna", lastName: "Kim", userType: "Student", email: "kimhyuna@coyote.csusb.edu", lastAccessed: DateTime.new(2020,3,1,8,37,48,"-07:00"), password_digest: 1234, units: 165.00, gradCatalogYear: 2017)
User.create(schoolId: 2 ,firstName: "Hyolyn", lastName: "Kim", userType: "Student", email: "kimhyolyn@coyote.csusb.edu", lastAccessed: DateTime.new(2020,3,1,8,37,48,"-07:00"), password_digest: 1234, units: 160.00, gradCatalogYear: 2017)
User.create(schoolId: 3 ,firstName: "Profe", lastName: "Gonzalez", userType: "Student", email: "profgonzalez@coyote.csusb.edu", lastAccessed: DateTime.new(2020,3,1,8,37,48,"-07:00"), password_digest: 1234, units: 17.00, gradCatalogYear: 2017)

#create a major
#Major.create(name: , catalogYear:)
Major.create(name: "B.S. Computer Science", catalogYear: 2017)
Major.create(name: "B.S. Physics", catalogYear: 2017)

#create a requirement category
#Need E Lifelong & G Multicultural & F upper writing
#RequirementCategory.create(name: ,category: ,unitFulfillmentRequirement: )
RequirementCategory.create(name: "1. Written Communication" ,category: "A. Basic Skills Category", unitFulfillmentRequirement: 4)
RequirementCategory.create(name: "2. Oral Communication" ,category: "A. Basic Skills Category", unitFulfillmentRequirement: 4)
RequirementCategory.create(name: "3. Mathematics" ,category: "A. Basic Skills Category", unitFulfillmentRequirement: 4)
RequirementCategory.create(name: "4. Critical Thinking" ,category: "A. Basic Skills Category", unitFulfillmentRequirement: 4)
RequirementCategory.create(name: "1. Mathematics" ,category: "B. Natural Sciences Breadth Area", unitFulfillmentRequirement: 4)
RequirementCategory.create(name: "2. Life Sciences" ,category: "B. Natural Sciences Breadth Area", unitFulfillmentRequirement: 5)
RequirementCategory.create(name: "3. Physical Sciences" ,category: "B. Natural Sciences Breadth Area", unitFulfillmentRequirement: 5)
RequirementCategory.create(name: "4. Special Topics in Science and Technology" ,category: "B. Natural Sciences Breadth Area", unitFulfillmentRequirement: 2)
RequirementCategory.create(name: "5. Integrative Capstone in Natural Sciences" ,category: "B. Natural Sciences Breadth Area", unitFulfillmentRequirement: 4)
RequirementCategory.create(name: "1. Arts" ,category: "C. Humanities Breadth Area", unitFulfillmentRequirement: 4)
RequirementCategory.create(name: "2. Literature" ,category: "C. Humanities Breadth Area", unitFulfillmentRequirement: 4)
RequirementCategory.create(name: "3. Foreign Language or Literature in Translation" ,category: "C. Humanities Breadth Area", unitFulfillmentRequirement: 4)
RequirementCategory.create(name: "4. Philosophy" ,category: "C. Humanities Breadth Area", unitFulfillmentRequirement: 4)
RequirementCategory.create(name: "5. Integrative Capstone in the Humanities" ,category: "C. Humanities Breadth Area", unitFulfillmentRequirement: 4)
RequirementCategory.create(name: "1. American History and Civilization" ,category: "D. Social and Behavioral Sciences", unitFulfillmentRequirement: 4)
RequirementCategory.create(name: "2. American Institutions" ,category: "D. Social and Behavioral Sciences", unitFulfillmentRequirement: 4)
RequirementCategory.create(name: "3. World Cultures" ,category: "D. Social and Behavioral Sciences", unitFulfillmentRequirement: 4)
RequirementCategory.create(name: "4. Discipline Perspectives" ,category: "D. Social and Behavioral Sciences", unitFulfillmentRequirement: 4)
RequirementCategory.create(name: "5. Integrative Capstone in the Social and Behavioral Sciences" ,category: "D. Social and Behavioral Sciences", unitFulfillmentRequirement: 4)

#Computer Science
RequirementCategory.create(name: "Lower-division Requirements A" ,category: "Computer Science", unitFulfillmentRequirement: 43)
RequirementCategory.create(name: "Lower-division Requirements B" ,category: "Computer Science", unitFulfillmentRequirement: 5)
RequirementCategory.create(name: "Lower-division Requirements C" ,category: "Computer Science", unitFulfillmentRequirement: 5)
RequirementCategory.create(name: "Upper-division Requirements A" ,category: "Computer Science", unitFulfillmentRequirement: 50)
RequirementCategory.create(name: "Upper-division Requirements B" ,category: "Computer Science", unitFulfillmentRequirement: 4)
RequirementCategory.create(name: "Upper-division Requirements C" ,category: "Computer Science", unitFulfillmentRequirement: 16)

#Physics
RequirementCategory.create(name: "Lower-division Requirements" ,category: "Physics", unitFulfillmentRequirement: 53)
RequirementCategory.create(name: "Upper-division Requirements A" ,category: "Physics", unitFulfillmentRequirement: 45)
RequirementCategory.create(name: "Upper-division Requirements B" ,category: "Physics", unitFulfillmentRequirement: 5)
RequirementCategory.create(name: "Upper-division Requirements C" ,category: "Physics", unitFulfillmentRequirement: 4)

#Add requirement categories to majors (need to add more here)
temp_major = Major.find_by(name: "B.S. Computer Science", catalogYear: 2017)
add_gen_ed_req_2017(temp_major)
add_req_cat_to_major(temp_major, "Lower-division Requirements A", "Computer Science")
add_req_cat_to_major(temp_major, "Lower-division Requirements B", "Computer Science")
add_req_cat_to_major(temp_major, "Lower-division Requirements C", "Computer Science")
add_req_cat_to_major(temp_major, "Upper-division Requirements A", "Computer Science")
add_req_cat_to_major(temp_major, "Upper-division Requirements B", "Computer Science")
add_req_cat_to_major(temp_major, "Upper-division Requirements C", "Computer Science")

temp_major = Major.find_by(name: "B.S. Physics", catalogYear: 2017)
add_gen_ed_req_2017(temp_major)
add_req_cat_to_major(temp_major, "Lower-division Requirements", "Physics")
add_req_cat_to_major(temp_major, "Upper-division Requirements A", "Physics")
add_req_cat_to_major(temp_major, "Upper-division Requirements B", "Physics")
add_req_cat_to_major(temp_major, "Upper-division Requirements C", "Physics")

#create a course     (need to add more here)
#Course.create(name:, descr:, genre:, units:, classStandingUnitRequirement:, divisionType:)
Course.create(name: "PHYS 221", descr: "General Physics I", genre: "Physics", units: 5, classStandingUnitRequirement: 0, divisionType: "lower")
Course.create(name: "PHYS 222", descr: "General Physics II", genre: "Physics", units: 5, classStandingUnitRequirement: 0, divisionType: "lower")
Course.create(name: "PHYS 223", descr: "General Physics III", genre: "Physics", units: 5, classStandingUnitRequirement: 0, divisionType: "lower")
Course.create(name: "PHYS 224", descr: "General Physics IV", genre: "Physics", units: 3, classStandingUnitRequirement: 0, divisionType: "lower")
Course.create(name: "PHYS 225", descr: "General Physics V", genre: "Physics", units: 3, classStandingUnitRequirement: 0, divisionType: "lower")
Course.create(name: "MATH 262", descr: "Applied Statistics", genre: "Math", units: 4, classStandingUnitRequirement: 0, divisionType: "lower")
Course.create(name: "CSE 201", descr: "Computer Science I", genre: "Computer Science", units: 4, classStandingUnitRequirement: 0, divisionType: "lower")
Course.create(name: "CSE 202", descr: "Computer Science II", genre: "Computer Science", units: 4, classStandingUnitRequirement: 0, divisionType: "lower")
Course.create(name: "CSE 310", descr: "Digital Logic", genre: "Computer Science", units: 5, classStandingUnitRequirement: 0, divisionType: "upper")
Course.create(name: "CSE 313", descr: "Machine Organization", genre: "Computer Science", units: 4, classStandingUnitRequirement: 0, divisionType: "upper")
Course.create(name: "CSE 330", descr: "Data Structures", genre: "Computer Science", units: 4, classStandingUnitRequirement: 0, divisionType: "upper")
Course.create(name: "CSE 401", descr: "Contemporary Computer Architecture", genre: "Computer Science", units: 5, classStandingUnitRequirement: 0, divisionType: "upper")
Course.create(name: "CSE 405", descr: "Server Programming", genre: "Computer Science", units: 4, classStandingUnitRequirement: 0, divisionType: "upper")
Course.create(name:"ENG 104A", descr:"Stretch Composition III", genre: "English", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"ENG 104B", descr:"Stretch Composition III for Multilingual Students", genre: "English", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"ENG 106A", descr:"Accelerated Stretch Composition II", genre: "English", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"ENG 106B", descr:"Accelerated Stretch Composition II for Multilingual Students", genre: "English", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"ENG 107", descr:"Advanced First-Year Composition", genre: "English", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"COMM 120", descr:"Oral Communication", genre: "Communication", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"MATH 110", descr:"College Algebra", genre: "Mathematics", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"MATH 115", descr:"The Ideas of Mathematics", genre: "Mathematics", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"MATH 120", descr:"Pre-Calculus Mathematics", genre: "Mathematics", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"MATH 165", descr:"Introductory Statistics and Hypothesis Testing", genre: "Mathematics", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"MATH 192", descr:"Methods of Calculus", genre: "Mathematics", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"MATH 211", descr:"Basic Concepts of Calculus", genre: "Mathematics", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"COMM 180", descr:"Critical Thinking Through Argumentation", genre: "Communication", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"MATH 180", descr:"Critical Thinking Through Applications of Mathematical Logic", genre: "Mathematics", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"PHIL 105", descr:"Critical Thinking Through Argument Analysis", genre: "Philosophy", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"PHIL 200", descr:"Critical Thinking Through Symbolic Logic", genre: "Philosophy", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"PSYC 105", descr:"Critical Thinking Through Problems Analysis", genre: "Psychology", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"SOC 180", descr:"Critical Thinking about Social Problems", genre: "Sociology", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"BIOL 100", descr:"Topics in Biology", genre: "Biology", units: 5, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"BIOL 202", descr:"Biology of Population", genre: "Biology", units: 5, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"HSCI 120", descr:"Health and Society: An Ecological Approach", genre: "Health Science", units: 5, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"ASTR 103", descr:"Descriptive Astronomy", genre: "Astronomy", units: 5, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"CHEM 100", descr:"Chemistry in the Modern World", genre: "Chemistry", units: 5, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"CHEM 205", descr:"Fundamentals of Chemistry I: General Chemistry", genre: "Chemistry", units: 5, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"CHEM 215", descr:"General Chemistry I: Atomic Structure and Chemical Bonding", genre: "Chemistry", units: 6, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"GEOG 103", descr:"Physical Geography", genre: "Geography", units: 5, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"GEOL 101", descr:"Introductory Geology", genre: "Geology", units: 5, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"PHYS 100", descr:"Physics in the Modern World", genre: "Physics", units: 5, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"PHYS 121", descr:"Basic Concepts of Physics I", genre: "Physics", units: 5, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"BIOL 216", descr:"Genetics and Society", genre: "Biology", units: 2, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"BIOL 217", descr:"Biology of Sexually Transmitted Diseases", genre: "Biology", units: 2, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"CHEM 105", descr:"Chemicals in Our Environment", genre: "Chemistry", units: 2, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"CSE 122", descr: "Bioinformatics", genre: "Computer Science", units: 2, classStandingUnitRequirement: 0, divisionType: "lower")
Course.create(name:"CSE 129", descr: "Science, Computing and Society", genre: "Computer Science", units: 2, classStandingUnitRequirement: 0, divisionType: "lower")
Course.create(name:"CSE 208", descr: "Introduction to Computer Engineering Design", genre: "Computer Science", units: 2, classStandingUnitRequirement: 0, divisionType: "lower")
Course.create(name:"GEOL 205", descr:"Volcanic Hazards, Surveillance and Prediction", genre: "Geology", units: 2, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"GEOL 210", descr:"Earthquakes: Science and Public Policy", genre: "Geology", units: 2, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"CSE 408", descr:"Sustainable Engineering Design", genre: "Computer Science", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"NSCI 300", descr:"Science and Technology", genre: "Natural Science", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"NSCI 310", descr:"Science and Technology", genre: "Natural Science", units: 4, classStandingUnitRequirement: 90, divisionType:"lower")
Course.create(name:"NSCI 314", descr:"Life in the Cosmos", genre: "Natural Science", units: 4, classStandingUnitRequirement: 90, divisionType:"lower")
Course.create(name:"NSCI 315", descr:"Natural Disasters", genre: "Natural Science", units: 4, classStandingUnitRequirement: 90, divisionType:"lower")
Course.create(name:"NSCI 320", descr:"Energy", genre: "Natural Science", units: 4, classStandingUnitRequirement: 90, divisionType:"lower")
Course.create(name:"NSCI 325", descr:"Perspectives on Gender", genre: "Natural Science", units: 4, classStandingUnitRequirement: 90, divisionType:"lower")
Course.create(name:"NSCI 351", descr:"Health and Human Ecology", genre: "Natural Science", units: 4, classStandingUnitRequirement: 90, divisionType:"lower")
Course.create(name:"NSCI 360", descr:"Legacy of Life", genre: "Natural Science", units: 4, classStandingUnitRequirement: 90, divisionType:"lower")
Course.create(name:"ART 200", descr:"Studies in Art", genre: "Art", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"HUM 180", descr:"The Art of Film", genre: "Humanities", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"MUS 180", descr:"Studies in Music", genre: "Music", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"TA 260", descr:" Introduction to Theatre", genre: "Theater Arts", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"ENG 110", descr:"World Literature I", genre: "English", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"ENG 111", descr:"World Literature II", genre: "English", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"ENG 160", descr:"World Drama", genre: "English", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"ENG 170", descr:"Studies in Literature", genre: "English", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"TA 160", descr:"World Drama", genre: "Theater Arts", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"TA 212", descr:"Oral Interpretation of Literature", genre: "Theater Arts", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"KOR 101", descr:"College Korean I", genre: "Korean", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"KOR 102", descr:"College Korean II", genre: "Korean", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"KOR 103", descr:"College Korean III", genre: "Korean", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"PHIL 190", descr:"Introduction to Philosophical Issues", genre: "Philosophy", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"PHIL 191", descr:"Introduction to Ethics", genre: "Philosophy", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"PHIL 192", descr:"Introduction to Philosophy of Religion", genre: "Philosophy", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"PHIL 193", descr:"Introduction to Eastern Philosophy", genre: "Philosophy", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"PHIL 194", descr:"Introduction to Knowledge and Reality", genre: "Philosophy", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"HUM 319", descr:"Myth, Metaphor and Symbol", genre: "Humanities", units: 4, classStandingUnitRequirement: 90, divisionType:"lower")
Course.create(name:"HUM 325", descr:"Perspectives on Gender", genre: "Humanities", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"HUM 328", descr:"Asian Cultural Traditions", genre: "Humanities", units: 4, classStandingUnitRequirement: 90, divisionType:"lower")
Course.create(name:"HUM 330", descr:"Arts and Ideas", genre: "Humanities", units: 4, classStandingUnitRequirement: 90, divisionType:"lower")
Course.create(name:"HUM 335", descr:"The Origin and Contemporary Role of Latino Culture", genre: "Humanities", units: 4, classStandingUnitRequirement: 90, divisionType:"lower")
Course.create(name:"HUM 340", descr:"Interpretation and Values", genre: "Humanities", units: 4, classStandingUnitRequirement: 90, divisionType:"lower")
Course.create(name:"HUM 344", descr:"Ideas in American Culture", genre: "Humanities", units: 4, classStandingUnitRequirement: 90, divisionType:"lower")
Course.create(name:"HUM 350", descr:"The Cultures of American Childhood", genre: "Humanities", units: 4, classStandingUnitRequirement: 90, divisionType:"lower")
Course.create(name:"HUM 370", descr:"African Heritage in the Arts", genre: "Humanities", units: 4, classStandingUnitRequirement: 90, divisionType:"lower")
Course.create(name:"HUM 375", descr:"The World of Islam", genre: "Humanities", units: 4, classStandingUnitRequirement: 90, divisionType:"lower")
Course.create(name:"HUM 380", descr:"Comparative Studies in the Arts: East and West", genre: "Humanities", units: 4, classStandingUnitRequirement: 90, divisionType:"lower")
Course.create(name:"HUM 385", descr:"A Cultural History of Fashion", genre: "Humanities", units: 4, classStandingUnitRequirement: 90, divisionType:"lower")
Course.create(name:"HIST 146", descr:"American Civilization", genre: "History", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"HIST 200", descr:"United States History to 1877", genre: "History", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"HIST 201", descr:"United States History, 1877 to the Present", genre: "History", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"PSCI 203", descr:"American Government", genre: "Political Science", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"ANTH 140", descr:"World Civilizations I, the Rise of Civilization", genre: "Anthropology", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"HIST 140", descr:"World Civilizations I, the Rise of Civilization", genre: "History", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"HIST 142", descr:"World Civilizations II, the Civilizations of the East and West", genre: "History", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"HIST 144", descr:"World Civilizations III", genre: "History", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"SSCI 165", descr:"Regions and Peoples of the World", genre: "Social Science", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"ANTH 100", descr:"Introduction to Anthropology: Human Evolution", genre: "Anthropology", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"ANTH 102", descr:"Introduction to Anthropology: Culture and Society", genre: "Anthropology", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"ECON 104", descr:"Economics of Social Issues", genre: "Economics", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"ES 100", descr:"Ethnicity and Race in America", genre: "Ethnic Studies", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"GEOG 100", descr:"Introduction to Human Geography", genre: "Geography", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"GSS 100", descr:"Introduction to Women's Studies", genre: "Gender and Sexuality Studies", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"GSS 101", descr:"Introduction to Masculinity Studies", genre: "Gender and Sexuality Studies", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"GSS 103", descr:"Introduction to Lesbian, Gay, Bisexual, and Transgender Studies", genre: "Gender and Sexuality Studies", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"PSYC 100", descr:"Introduction to Psychology", genre: "Psychology", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"SOC 100", descr:"The Study of Society", genre: "Sociology", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"SSCI 300", descr:"Nonwestern World", genre: "Social Sciences", units: 4, classStandingUnitRequirement: 90, divisionType:"lower")
Course.create(name:"SSCI 304", descr:"Contemporary Latin America", genre: "Social Sciences", units: 4, classStandingUnitRequirement: 90, divisionType:"lower")
Course.create(name:"SSCI 315", descr:"Cultural Adaptation: The Quest for Survival", genre: "Social Sciences", units: 4, classStandingUnitRequirement: 90, divisionType:"lower")
Course.create(name:"SSCI 316", descr:"Race and Racism", genre: "Social Sciences", units: 4, classStandingUnitRequirement: 90, divisionType:"lower")
Course.create(name:"SSCI 320", descr:"Understanding Capitalism", genre: "Social Sciences", units: 4, classStandingUnitRequirement: 90, divisionType:"lower")
Course.create(name:"SSCI 321", descr:"Urbanization and the Urban Environment", genre: "Social Sciences", units: 4, classStandingUnitRequirement: 90, divisionType:"lower")
Course.create(name:"SSCI 325", descr:"Perspectives on Gender", genre: "Social Sciences", units: 4, classStandingUnitRequirement: 90, divisionType:"lower")
Course.create(name:"SSCI 345", descr:"Religious Expression in America", genre: "Social Sciences", units: 4, classStandingUnitRequirement: 90, divisionType:"lower")
Course.create(name:"SSCI 350", descr:"Roots of Modern Racism in America", genre: "Social Sciences", units: 4, classStandingUnitRequirement: 90, divisionType:"lower")
Course.create(name:"MATH 212", descr:"Calculus II", genre: "Mathematics", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"MATH 213", descr:"Calculus III", genre: "Mathematics", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"MATH 272", descr:"Discrete Mathematics", genre: "Mathematics", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"BIOL 200", descr:"Biology of the Cell", genre: "Biology", units: 5, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"CSE 320", descr:"Programming Languages", genre: "Computer Science", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"CSE 431", descr:"Algorithm Analysis", genre: "Computer Science", units: 4, classStandingUnitRequirement: 0, divisionType:"upper")
Course.create(name:"CSE 460", descr:"Operating Systems", genre: "Computer Science", units: 4, classStandingUnitRequirement: 0, divisionType:"upper")
Course.create(name:"CSE 461", descr:"Advanced Operating Systems", genre: "Computer Science", units: 4, classStandingUnitRequirement: 0, divisionType:"upper")
Course.create(name:"CSE 488", descr:"Ethics and the Computing Professional", genre: "Computer Science", units: 2, classStandingUnitRequirement: 135, divisionType:"upper")
Course.create(name:"CSE 489", descr:"Senior Seminar", genre: "Computer Science", units: 2, classStandingUnitRequirement: 135, divisionType:"upper")
Course.create(name:"CSE 500", descr:"Introduction to Formal Languages and Automata", genre: "Computer Science", units: 4, classStandingUnitRequirement: 0, divisionType:"upper")
Course.create(name:"CSE 570", descr:"Compilers", genre: "Computer Science", units: 4, classStandingUnitRequirement: 0, divisionType:"upper")
Course.create(name:"MATH 372", descr:"Combinatorics", genre: "Mathematics", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"CSE 455", descr:"Software Engineering", genre: "Computer Science", units: 4, classStandingUnitRequirement: 0, divisionType:"upper")
Course.create(name:"CSE 555", descr:"Software Design and Architecture", genre: "Computer Science", units: 4, classStandingUnitRequirement: 0, divisionType:"upper")
Course.create(name:"CSE 441", descr:"Game Programming", genre: "Computer Science", units: 4, classStandingUnitRequirement: 0, divisionType:"upper")
Course.create(name:"CSE 516", descr:"Machine Learning", genre: "Computer Science", units: 4, classStandingUnitRequirement: 0, divisionType:"upper")
Course.create(name:"CSE 535", descr:"Numerical Computation", genre: "Computer Science", units: 4, classStandingUnitRequirement: 0, divisionType:"upper")
Course.create(name:"CSE 541", descr:"Robotics and Control", genre: "Computer Science", units: 4, classStandingUnitRequirement: 0, divisionType:"upper")
Course.create(name:"CSE 512", descr:"Introduction to Artificial Intelligence", genre: "Computer Science", units: 4, classStandingUnitRequirement: 0, divisionType:"upper")
Course.create(name: "PHYS 150", descr: "Introductory Electronics", genre: "Physics", units: 5, classStandingUnitRequirement: 0, divisionType: "lower")
Course.create(name: "CHEM 216", descr: "General Chemistry II: Principles of Chemical Reactions", genre: "Chemistry", units: 6, classStandingUnitRequirement: 0, divisionType: "lower")
Course.create(name:"MATH 251", descr:"Multivariable Calculus I", genre: "Mathematics", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"MATH 252", descr:"Multivariable Calculus II", genre: "Mathematics", units: 4, classStandingUnitRequirement: 0, divisionType:"lower")
Course.create(name:"MATH 331", descr:"Linear Algebra", genre: "Mathematics", units: 4, classStandingUnitRequirement: 0, divisionType:"upper")
Course.create(name: "PHYS 306", descr: "Classical Mechanics I", genre: "Physics", units: 4, classStandingUnitRequirement: 0, divisionType: "upper")
Course.create(name: "PHYS 307", descr: "Classical Mechanics II", genre: "Physics", units: 4, classStandingUnitRequirement: 0, divisionType: "upper")
Course.create(name: "PHYS 313", descr: "Electrodynamics I", genre: "Physics", units: 4, classStandingUnitRequirement: 0, divisionType: "upper")
Course.create(name: "PHYS 314", descr: "Electrodynamics II", genre: "Physics", units: 4, classStandingUnitRequirement: 0, divisionType: "upper")
Course.create(name: "PHYS 324", descr: "Statistical and Thermal Physics", genre: "Physics", units: 4, classStandingUnitRequirement: 0, divisionType: "upper")
Course.create(name: "PHYS 373", descr: "Mathematical Methods of Physics I", genre: "Physics", units: 4, classStandingUnitRequirement: 0, divisionType: "upper")
Course.create(name: "PHYS 398", descr: "Junior Assessment", genre: "Physics", units: 1, classStandingUnitRequirement: 0, divisionType: "upper")
Course.create(name: "PHYS 421", descr: "Quantum Mechanics I", genre: "Physics", units: 4, classStandingUnitRequirement: 0, divisionType: "upper")
Course.create(name: "PHYS 422", descr: "Quantum Mechanics II", genre: "Physics", units: 4, classStandingUnitRequirement: 0, divisionType: "upper")
Course.create(name: "PHYS 430", descr: "Advanced Physics Laboratory", genre: "Physics", units: 4, classStandingUnitRequirement: 0, divisionType: "upper")
Course.create(name: "PHYS 473", descr: "Mathematical Methods of Physics II", genre: "Physics", units: 4, classStandingUnitRequirement: 0, divisionType: "upper")
Course.create(name: "PHYS 315", descr: "Introduction to Modern Optics", genre: "Physics", units: 4, classStandingUnitRequirement: 0, divisionType: "upper")
Course.create(name: "PHYS 318", descr: "Materials Science and Engineering", genre: "Physics", units: 4, classStandingUnitRequirement: 0, divisionType: "upper")
Course.create(name: "PHYS 370", descr: "Introduction to Astrophysics", genre: "Physics", units: 4, classStandingUnitRequirement: 0, divisionType: "upper")
Course.create(name: "PHYS 450", descr: "Solid State Physics", genre: "Physics", units: 4, classStandingUnitRequirement: 0, divisionType: "upper")
Course.create(name: "PHYS 461", descr: "Introduction to Nuclear Physics", genre: "Physics", units: 4, classStandingUnitRequirement: 0, divisionType: "upper")
Course.create(name: "PHYS 463", descr: "Introduction to Elementary Particle Physics", genre: "Physics", units: 4, classStandingUnitRequirement: 0, divisionType: "upper")
Course.create(name: "PHYS 480A", descr: "Topics in Classical Physics: Mechanics", genre: "Physics", units: 4, classStandingUnitRequirement: 0, divisionType: "upper")
Course.create(name: "PHYS 480B", descr: "Topics in Classical Physics: Electrodynamics", genre: "Physics", units: 4, classStandingUnitRequirement: 0, divisionType: "upper")
Course.create(name: "PHYS 485A", descr: "Topics in Contemporary Physics", genre: "Physics", units: 1, classStandingUnitRequirement: 0, divisionType: "upper")
Course.create(name: "PHYS 485B", descr: "Topics in Contemporary Physics", genre: "Physics", units: 2, classStandingUnitRequirement: 0, divisionType: "upper")
Course.create(name: "PHYS 485C", descr: "Topics in Contemporary Physics", genre: "Physics", units: 3, classStandingUnitRequirement: 0, divisionType: "upper")
Course.create(name: "PHYS 485D", descr: "Topics in Contemporary Physics", genre: "Physics", units: 4, classStandingUnitRequirement: 0, divisionType: "upper")
Course.create(name: "PHYS 573", descr: "Mathematical Methods of Physics III", genre: "Physics", units: 4, classStandingUnitRequirement: 0, divisionType: "upper")
Course.create(name: "PHYS 590", descr: "Physics Seminar", genre: "Physics", units: 1, classStandingUnitRequirement: 0, divisionType: "upper")

#adding courses to requirement categories
temp_req_cat = RequirementCategory.find_by(name: "1. Written Communication" ,category: "A. Basic Skills Category")
add_course_to_req_cat(temp_req_cat, "ENG 104A")
add_course_to_req_cat(temp_req_cat, "ENG 104B")
add_course_to_req_cat(temp_req_cat, "ENG 106A")
add_course_to_req_cat(temp_req_cat, "ENG 106B")
add_course_to_req_cat(temp_req_cat, "ENG 107")

temp_req_cat = RequirementCategory.find_by(name: "2. Oral Communication" ,category: "A. Basic Skills Category")
add_course_to_req_cat(temp_req_cat, "COMM 180")

temp_req_cat = RequirementCategory.find_by(name: "3. Mathematics" ,category: "A. Basic Skills Category")
add_course_to_req_cat(temp_req_cat, "MATH 110")
add_course_to_req_cat(temp_req_cat, "MATH 115")
add_course_to_req_cat(temp_req_cat, "MATH 120")
add_course_to_req_cat(temp_req_cat, "MATH 165")
add_course_to_req_cat(temp_req_cat, "MATH 192")
add_course_to_req_cat(temp_req_cat, "MATH 211")

temp_req_cat = RequirementCategory.find_by(name: "4. Critical Thinking" ,category: "A. Basic Skills Category")
add_course_to_req_cat(temp_req_cat, "COMM 180")
add_course_to_req_cat(temp_req_cat, "MATH 180")
add_course_to_req_cat(temp_req_cat, "PHIL 105")
add_course_to_req_cat(temp_req_cat, "PHIL 200")
add_course_to_req_cat(temp_req_cat, "PSYC 105")
add_course_to_req_cat(temp_req_cat, "SOC 180")

#not duplicate different category than mathematics above
temp_req_cat = RequirementCategory.find_by(name: "1. Mathematics" ,category: "B. Natural Sciences Breadth Area")
add_course_to_req_cat(temp_req_cat, "MATH 110")
add_course_to_req_cat(temp_req_cat, "MATH 115")
add_course_to_req_cat(temp_req_cat, "MATH 120")
add_course_to_req_cat(temp_req_cat, "MATH 165")
add_course_to_req_cat(temp_req_cat, "MATH 192")
add_course_to_req_cat(temp_req_cat, "MATH 211")

temp_req_cat = RequirementCategory.find_by(name: "2. Life Sciences" ,category: "B. Natural Sciences Breadth Area")
add_course_to_req_cat(temp_req_cat, "BIOL 100")
add_course_to_req_cat(temp_req_cat, "BIOL 202")
add_course_to_req_cat(temp_req_cat, "HSCI 120")

temp_req_cat = RequirementCategory.find_by(name: "3. Physical Sciences" ,category: "B. Natural Sciences Breadth Area")
add_course_to_req_cat(temp_req_cat, "ASTR 103")
add_course_to_req_cat(temp_req_cat, "CHEM 100")
add_course_to_req_cat(temp_req_cat, "CHEM 205")
add_course_to_req_cat(temp_req_cat, "CHEM 215")
add_course_to_req_cat(temp_req_cat, "GEOG 103")
add_course_to_req_cat(temp_req_cat, "GEOL 101")
add_course_to_req_cat(temp_req_cat, "PHYS 100")
add_course_to_req_cat(temp_req_cat, "PHYS 121")
add_course_to_req_cat(temp_req_cat, "PHYS 221")

temp_req_cat = RequirementCategory.find_by(name: "4. Special Topics in Science and Technology" ,category: "B. Natural Sciences Breadth Area")
add_course_to_req_cat(temp_req_cat, "BIOL 216")
add_course_to_req_cat(temp_req_cat, "BIOL 217")
add_course_to_req_cat(temp_req_cat, "CHEM 105")
add_course_to_req_cat(temp_req_cat, "CSE 122")
add_course_to_req_cat(temp_req_cat, "CSE 129")
add_course_to_req_cat(temp_req_cat, "CSE 208")
add_course_to_req_cat(temp_req_cat, "GEOL 205")
add_course_to_req_cat(temp_req_cat, "GEOL 210")

temp_req_cat = RequirementCategory.find_by(name: "5. Integrative Capstone in Natural Sciences" ,category: "B. Natural Sciences Breadth Area")
add_course_to_req_cat(temp_req_cat, "CSE 408")
add_course_to_req_cat(temp_req_cat, "NSCI 300")
add_course_to_req_cat(temp_req_cat, "NSCI 310")
add_course_to_req_cat(temp_req_cat, "NSCI 314")
add_course_to_req_cat(temp_req_cat, "NSCI 315")
add_course_to_req_cat(temp_req_cat, "NSCI 320")
add_course_to_req_cat(temp_req_cat, "NSCI 325")
add_course_to_req_cat(temp_req_cat, "NSCI 351")
add_course_to_req_cat(temp_req_cat, "NSCI 360")

temp_req_cat = RequirementCategory.find_by(name: "1. Arts" ,category: "C. Humanities Breadth Area")
add_course_to_req_cat(temp_req_cat, "ART 200")
add_course_to_req_cat(temp_req_cat, "HUM 180")
add_course_to_req_cat(temp_req_cat, "MUS 180")
add_course_to_req_cat(temp_req_cat, "TA 260")

temp_req_cat = RequirementCategory.find_by(name: "2. Literature" ,category: "C. Humanities Breadth Area")
add_course_to_req_cat(temp_req_cat, "ENG 110")
add_course_to_req_cat(temp_req_cat, "ENG 111")
add_course_to_req_cat(temp_req_cat, "ENG 160")
add_course_to_req_cat(temp_req_cat, "ENG 170")
add_course_to_req_cat(temp_req_cat, "TA 160")
add_course_to_req_cat(temp_req_cat, "TA 212")

#incomplete
temp_req_cat = RequirementCategory.find_by(name: "3. Foreign Language or Literature in Translation" ,category: "C. Humanities Breadth Area")
add_course_to_req_cat(temp_req_cat, "KOR 103")

temp_req_cat = RequirementCategory.find_by(name: "4. Philosophy" ,category: "C. Humanities Breadth Area")
add_course_to_req_cat(temp_req_cat, "PHIL 190")
add_course_to_req_cat(temp_req_cat, "PHIL 191")
add_course_to_req_cat(temp_req_cat, "PHIL 192")
add_course_to_req_cat(temp_req_cat, "PHIL 193")
add_course_to_req_cat(temp_req_cat, "PHIL 194")

temp_req_cat = RequirementCategory.find_by(name: "5. Integrative Capstone in the Humanities" ,category: "C. Humanities Breadth Area")
add_course_to_req_cat(temp_req_cat, "HUM 319")
add_course_to_req_cat(temp_req_cat, "HUM 325")
add_course_to_req_cat(temp_req_cat, "HUM 328")
add_course_to_req_cat(temp_req_cat, "HUM 330")
add_course_to_req_cat(temp_req_cat, "HUM 335")
add_course_to_req_cat(temp_req_cat, "HUM 340")
add_course_to_req_cat(temp_req_cat, "HUM 344")
add_course_to_req_cat(temp_req_cat, "HUM 350")
add_course_to_req_cat(temp_req_cat, "HUM 370")
add_course_to_req_cat(temp_req_cat, "HUM 375")
add_course_to_req_cat(temp_req_cat, "HUM 380")
add_course_to_req_cat(temp_req_cat, "HUM 385")

temp_req_cat = RequirementCategory.find_by(name: "1. American History and Civilization" ,category: "D. Social and Behavioral Sciences")
add_course_to_req_cat(temp_req_cat, "HIST 146")
add_course_to_req_cat(temp_req_cat, "HIST 200")
add_course_to_req_cat(temp_req_cat, "HIST 201")

temp_req_cat = RequirementCategory.find_by(name: "2. American Institutions" ,category: "D. Social and Behavioral Sciences")
add_course_to_req_cat(temp_req_cat, "PSCI 203")

temp_req_cat = RequirementCategory.find_by(name: "3. World Cultures" ,category: "D. Social and Behavioral Sciences")
add_course_to_req_cat(temp_req_cat, "ANTH 140")
add_course_to_req_cat(temp_req_cat, "HIST 140")
add_course_to_req_cat(temp_req_cat, "HIST 142")
add_course_to_req_cat(temp_req_cat, "HIST 144")
add_course_to_req_cat(temp_req_cat, "SSCI 165")

temp_req_cat = RequirementCategory.find_by(name: "4. Discipline Perspectives" ,category: "D. Social and Behavioral Sciences", unitFulfillmentRequirement: 4)
add_course_to_req_cat(temp_req_cat, "ANTH 100")
add_course_to_req_cat(temp_req_cat, "ANTH 102")
add_course_to_req_cat(temp_req_cat, "ECON 104")
add_course_to_req_cat(temp_req_cat, "ES 100")
add_course_to_req_cat(temp_req_cat, "GEOG 100")
add_course_to_req_cat(temp_req_cat, "GSS 100")
add_course_to_req_cat(temp_req_cat, "GSS 101")
add_course_to_req_cat(temp_req_cat, "GSS 103")
add_course_to_req_cat(temp_req_cat, "PSYC 100")
add_course_to_req_cat(temp_req_cat, "SOC 100")

temp_req_cat = RequirementCategory.find_by(name: "5. Integrative Capstone in the Social and Behavioral Sciences" ,category: "D. Social and Behavioral Sciences", unitFulfillmentRequirement: 4)
add_course_to_req_cat(temp_req_cat, "SSCI 300")
add_course_to_req_cat(temp_req_cat, "SSCI 304")
add_course_to_req_cat(temp_req_cat, "SSCI 315")
add_course_to_req_cat(temp_req_cat, "SSCI 316")
add_course_to_req_cat(temp_req_cat, "SSCI 320")
add_course_to_req_cat(temp_req_cat, "SSCI 321")
add_course_to_req_cat(temp_req_cat, "SSCI 325")
add_course_to_req_cat(temp_req_cat, "SSCI 345")
add_course_to_req_cat(temp_req_cat, "SSCI 350")

temp_req_cat = RequirementCategory.find_by(name: "Lower-division Requirements A" ,category: "Computer Science")
add_course_to_req_cat(temp_req_cat, "CSE 201")
add_course_to_req_cat(temp_req_cat, "CSE 202")
add_course_to_req_cat(temp_req_cat, "MATH 211")
add_course_to_req_cat(temp_req_cat, "MATH 212")
add_course_to_req_cat(temp_req_cat, "MATH 213")
add_course_to_req_cat(temp_req_cat, "MATH 262")
add_course_to_req_cat(temp_req_cat, "MATH 272")
add_course_to_req_cat(temp_req_cat, "PHYS 221")
add_course_to_req_cat(temp_req_cat, "PHYS 222")
add_course_to_req_cat(temp_req_cat, "PHYS 223")

temp_req_cat = RequirementCategory.find_by(name: "Lower-division Requirements B" ,category: "Computer Science")
add_course_to_req_cat(temp_req_cat, "BIOL 100")
add_course_to_req_cat(temp_req_cat, "BIOL 200")

#to do add course with lab in ASTRN BIO CHEM GEO PHYS (not PHYS 100 121 122 123)
temp_req_cat = RequirementCategory.find_by(name: "Lower-division Requirements C" ,category: "Computer Science")
add_course_to_req_cat(temp_req_cat, "ASTR 103")
add_course_to_req_cat(temp_req_cat, "BIOL 100")
add_course_to_req_cat(temp_req_cat, "CHEM 205")
add_course_to_req_cat(temp_req_cat, "PHYS 150")

temp_req_cat = RequirementCategory.find_by(name: "Upper-division Requirements A" ,category: "Computer Science")
add_course_to_req_cat(temp_req_cat, "CSE 310")
add_course_to_req_cat(temp_req_cat, "CSE 313")
add_course_to_req_cat(temp_req_cat, "CSE 320")
add_course_to_req_cat(temp_req_cat, "CSE 330")
add_course_to_req_cat(temp_req_cat, "CSE 401")
add_course_to_req_cat(temp_req_cat, "CSE 431")
add_course_to_req_cat(temp_req_cat, "CSE 460")
add_course_to_req_cat(temp_req_cat, "CSE 461")
add_course_to_req_cat(temp_req_cat, "CSE 488")
add_course_to_req_cat(temp_req_cat, "CSE 489")
add_course_to_req_cat(temp_req_cat, "CSE 500")
add_course_to_req_cat(temp_req_cat, "CSE 570")
add_course_to_req_cat(temp_req_cat, "MATH 372")

temp_req_cat = RequirementCategory.find_by(name: "Upper-division Requirements B" ,category: "Computer Science")
add_course_to_req_cat(temp_req_cat, "CSE 455")
add_course_to_req_cat(temp_req_cat, "CSE 555")

#CSE 400 level and above not in previous comp sci req cats
temp_req_cat = RequirementCategory.find_by(name: "Upper-division Requirements C" ,category: "Computer Science")
add_course_to_req_cat(temp_req_cat, "CSE 441")
add_course_to_req_cat(temp_req_cat, "CSE 512")
add_course_to_req_cat(temp_req_cat, "CSE 516")
add_course_to_req_cat(temp_req_cat, "CSE 535")
add_course_to_req_cat(temp_req_cat, "CSE 541")

#Physics
temp_req_cat = RequirementCategory.find_by(name: "Lower-division Requirements" ,category: "Physics")
add_course_to_req_cat(temp_req_cat, "CHEM 215")
add_course_to_req_cat(temp_req_cat, "CHEM 216")
add_course_to_req_cat(temp_req_cat, "MATH 211")
add_course_to_req_cat(temp_req_cat, "MATH 212")
add_course_to_req_cat(temp_req_cat, "MATH 213")
add_course_to_req_cat(temp_req_cat, "MATH 251")
add_course_to_req_cat(temp_req_cat, "MATH 252")
add_course_to_req_cat(temp_req_cat, "PHYS 221")
add_course_to_req_cat(temp_req_cat, "PHYS 222")
add_course_to_req_cat(temp_req_cat, "PHYS 223")
add_course_to_req_cat(temp_req_cat, "PHYS 224")
add_course_to_req_cat(temp_req_cat, "PHYS 225")

temp_req_cat = RequirementCategory.find_by(name: "Upper-division Requirements A" ,category: "Physics")
add_course_to_req_cat(temp_req_cat, "MATH 331")
add_course_to_req_cat(temp_req_cat, "PHYS 306")
add_course_to_req_cat(temp_req_cat, "PHYS 307")
add_course_to_req_cat(temp_req_cat, "PHYS 313")
add_course_to_req_cat(temp_req_cat, "PHYS 314")
add_course_to_req_cat(temp_req_cat, "PHYS 324")
add_course_to_req_cat(temp_req_cat, "PHYS 373")
add_course_to_req_cat(temp_req_cat, "PHYS 398")
add_course_to_req_cat(temp_req_cat, "PHYS 421")
add_course_to_req_cat(temp_req_cat, "PHYS 422")
add_course_to_req_cat(temp_req_cat, "PHYS 430")
add_course_to_req_cat(temp_req_cat, "PHYS 473")

temp_req_cat = RequirementCategory.find_by(name: "Upper-division Requirements B" ,category: "Physics")
add_course_to_req_cat(temp_req_cat, "PHYS 315")
add_course_to_req_cat(temp_req_cat, "PHYS 318")
add_course_to_req_cat(temp_req_cat, "PHYS 370")
add_course_to_req_cat(temp_req_cat, "PHYS 450")
add_course_to_req_cat(temp_req_cat, "PHYS 461")
add_course_to_req_cat(temp_req_cat, "PHYS 463")
add_course_to_req_cat(temp_req_cat, "PHYS 480A")
add_course_to_req_cat(temp_req_cat, "PHYS 480B")
add_course_to_req_cat(temp_req_cat, "PHYS 485A")
add_course_to_req_cat(temp_req_cat, "PHYS 485B")
add_course_to_req_cat(temp_req_cat, "PHYS 485C")
add_course_to_req_cat(temp_req_cat, "PHYS 485D")
add_course_to_req_cat(temp_req_cat, "PHYS 573")
add_course_to_req_cat(temp_req_cat, "PHYS 590")

temp_req_cat = RequirementCategory.find_by(name: "Upper-division Requirements C" ,category: "Physics")
add_course_to_req_cat(temp_req_cat, "CSE 202")
add_course_to_req_cat(temp_req_cat, "CSE 310")
add_course_to_req_cat(temp_req_cat, "CSE 313")
add_course_to_req_cat(temp_req_cat, "CSE 320")
add_course_to_req_cat(temp_req_cat, "CSE 330")

#create a term
#Term.create(name:, year:, isCurrentTerm: )
Term.create(name: "Spring", year: 2020, start_date: Date.new(2020,3,28), end_date: Date.new(2020,6,8), isCurrentTerm: true)

#Assign a course to a term through TermCourse
temp_course_term = Term.find_by(name: "Spring", year: 2020).term_courses.create(status: "open", courseType: "Lec", location:"Jack Brown Hall 102", instructor: "Staff", section: 1, classNumber: 43114, tuesday_start: 1200, tuesday_end: 1350, thursday_start: 1200, thursday_end: 1350)
temp_course = Course.find_by(name: "PHYS 223")
temp_course_term.course = temp_course
temp_course_term.save!

temp_course_term = Term.find_by(name: "Spring", year: 2020).term_courses.create(status: "open", courseType: "Lab", location:"Physical Sciences 124", instructor: "Staff", section: 2, classNumber: 43115, wednesday_start: 900, wednesday_end: 1150)
temp_course = Course.find_by(name: "PHYS 223")
temp_course_term.course = temp_course
temp_course_term.save!

temp_course_term = Term.find_by(name: "Spring", year: 2020).term_courses.create(status: "open", courseType: "Lec", location:"Jack Brown Hall 146", instructor: "Sonia Pervez-Gamboa", section: 1, classNumber: 43190, monday_start: 1800, monday_end: 1915, wednesday_start: 1800, wednesday_end: 1915)
temp_course = Course.find_by(name: "CSE 201")
temp_course_term.course = temp_course
temp_course_term.save!


temp_course_term = Term.find_by(name: "Spring", year: 2020).term_courses.create(status: "open", courseType: "Act", location:"Jack Brown Hall 358", instructor: "Sonia Pervez-Gamboa", section: 2, classNumber: 43191, monday_start: 1930, monday_end: 2020, wednesday_start: 1930, wednesday_end: 2020)
temp_course = Course.find_by(name: "CSE 201")
temp_course_term.course = temp_course
temp_course_term.save!

temp_course_term = Term.find_by(name: "Spring", year: 2020).term_courses.create(status: "closed", courseType: "Lec", location:"Jack Brown Hall 258", instructor: "Ernesto Gomez", section: 1, classNumber: 44318, monday_start: 1200, monday_end: 1350, wednesday_start: 1200, wednesday_end: 1350)
temp_course = Course.find_by(name: "CSE 401")
temp_course_term.course = temp_course
temp_course_term.save!

temp_course_term = Term.find_by(name: "Spring", year: 2020).term_courses.create(status: "waitlisted", courseType: "Act", location:"Jack Brown Hall 360", instructor: "Ernesto Gomez", section: 2, classNumber: 44341, monday_start: 1400, monday_end: 1645)
temp_course = Course.find_by(name: "CSE 401")
temp_course_term.course = temp_course
temp_course_term.save!

temp_course_term = Term.find_by(name: "Spring", year: 2020).term_courses.create(status: "waitlisted", courseType: "Act", location:"Jack Brown Hall 360", instructor: "Ernesto Gomez", section: 3, classNumber: 44179, wednesday_start: 1400, wednesday_end: 1645)
temp_course = Course.find_by(name: "CSE 401")
temp_course_term.course = temp_course
temp_course_term.save!

temp_course_term = Term.find_by(name: "Spring", year: 2020).term_courses.create(status: "open", courseType: "Lec", location:"Jack Brown Hall 109", instructor: "David Turner", section: 1, classNumber: 43401, tuesday_start: 830, tuesday_end: 950, thursday_start: 830, thursday_end: 950)
temp_course = Course.find_by(name: "CSE 405")
temp_course_term.course = temp_course
temp_course_term.save!

temp_course_term = Term.find_by(name: "Spring", year: 2020).term_courses.create(status: "open", courseType: "Act", location:"Jack Brown Hall 359", instructor: "David Turner", section: 2, classNumber: 43402, tuesday_start: 1000, tuesday_end: 1150)
temp_course = Course.find_by(name: "CSE 405")
temp_course_term.course = temp_course
temp_course_term.save!

temp_course_term = Term.find_by(name: "Spring", year: 2020).term_courses.create(status: "open", courseType: "Act", location:"TBD", instructor: "David Turner", section: 3, classNumber: 43403, thursday_start: 1000, thursday_end: 1150)
temp_course = Course.find_by(name: "CSE 405")
temp_course_term.course = temp_course
temp_course_term.save!

temp_course_term = Term.find_by(name: "Spring", year: 2020).term_courses.create(status: "open", courseType: "Lec", location:"Jack Brown Hall 262", instructor: "D Guthrey", section: 1, classNumber: 42631, tuesday_start:800, tuesday_end:950, thursday_start: 800, thursday_end: 950)
temp_course = Course.find_by(name: "MATH 262")
temp_course_term.course = temp_course
temp_course_term.save!

temp_course_term = Term.find_by(name: "Spring", year: 2020).term_courses.create(status: "waitlisted", courseType: "Lec", location:"Jack Brown Hall 262", instructor: "Lida Ahmadi", section: 2, classNumber: 43084, tuesday_start:1000, tuesday_end:1150, thursday_start: 1000, thursday_end: 1150)
temp_course = Course.find_by(name: "MATH 262")
temp_course_term.course = temp_course
temp_course_term.save!

temp_course_term = Term.find_by(name: "Spring", year: 2020).term_courses.create(status: "open", courseType: "Sem", location:"University Hall 241", instructor: "John Pate", section: 1, classNumber: 41880, monday_start:800, monday_end:910, wednesday_start:800, wednesday_end:910,friday_start:800, friday_end:910)
temp_course = Course.find_by(name: "COMM 180")
temp_course_term.course = temp_course
temp_course_term.save!

temp_course_term = Term.find_by(name: "Spring", year: 2020).term_courses.create(status: "open", courseType: "Sem", location:"University Hall 241", instructor: "John Pate", section: 2, classNumber: 41881, monday_start:920, monday_end:1030, wednesday_start:920, wednesday_end:1030,friday_start:920, friday_end:1030)
temp_course = Course.find_by(name: "COMM 180")
temp_course_term.course = temp_course
temp_course_term.save!

temp_course_term = Term.find_by(name: "Spring", year: 2020).term_courses.create(status: "open", courseType: "Sem", location:"University Hall 250", instructor: "Linda Tat", section: 3, classNumber: 41930, monday_start:240, monday_end:350, wednesday_start:240, wednesday_end:350,friday_start:240, friday_end:350)
temp_course = Course.find_by(name: "COMM 180")
temp_course_term.course = temp_course
temp_course_term.save!

temp_course_term = Term.find_by(name: "Spring", year: 2020).term_courses.create(status: "open", courseType: "Sem", location:"University Hall 241", instructor: "Victoria Luckner", section: 4, classNumber: 42146, tuesday_start:1000, tuesday_end:1150,thursday_start:1000, thursday_end:1150)
temp_course = Course.find_by(name: "COMM 180")
temp_course_term.course = temp_course
temp_course_term.save!

temp_course_term = Term.find_by(name: "Spring", year: 2020).term_courses.create(status: "open", courseType: "Lec", location:"Health and Physical Edu 124", instructor: "Kathleen Devlin", section: 1, classNumber: 42794, thursday_start:1200, thursday_end:1350)
temp_course = Course.find_by(name: "CSE 122")
temp_course_term.course = temp_course
temp_course_term.save!

temp_course_term = Term.find_by(name: "Spring", year: 2020).term_courses.create(status: "open", courseType: "Lec", location:"University Hall 106", instructor: "Joseph Madrid", section: 1, classNumber: 42774, thursday_start:800, thursday_end:950)
temp_course = Course.find_by(name: "BIOL 216")
temp_course_term.course = temp_course
temp_course_term.save!

temp_course_term = Term.find_by(name: "Spring", year: 2020).term_courses.create(status: "open", courseType: "Lec", location:"University Hall 250", instructor: "Jennifer Keys", section: 3, classNumber: 44152, monday_start:1200, monday_end:1310,wednesday_start:1200, wednesday_end:1310,friday_start:1200, friday_end:1310)
temp_course = Course.find_by(name: "ENG 110")
temp_course_term.course = temp_course
temp_course_term.save!



#Assign user with schoolId 3 a major and courses
temp_user = User.find_by(schoolId: 3)
add_major_to_user(temp_user, "B.S. Computer Science", 2017 )
add_course_to_user(temp_user, "CSE 201")
add_course_to_user(temp_user, "CSE 202")
add_course_to_user(temp_user, "CSE 310")
add_course_to_user(temp_user, "CSE 313")
add_course_to_user(temp_user, "CSE 330")
add_course_to_user(temp_user, "PHYS 221")
add_course_to_user(temp_user, "COMM 180")
add_course_to_user(temp_user, "ENG 104A")
add_course_to_user(temp_user, "MATH 110")
add_course_to_user(temp_user, "MATH 211")
add_course_to_user(temp_user, "CHEM 100")
add_course_to_user(temp_user, "BIOL 100")
add_course_to_user(temp_user, "ES 100")
add_course_to_user(temp_user, "CSE 512")




